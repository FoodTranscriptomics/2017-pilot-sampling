# Libraries ---------------------------------------------------------------
library(readxl)
library(dplyr)
library(purrr)
library(janitor)
library(lubridate)



# Read data ---------------------------------------------------------------
data <- read_excel("input/Fabrezan samples overview_fixed.xlsx",sheet = "Samples", skip = 2, trim_ws = TRUE, col_names = TRUE) %>%
        filter(!grepl("Red + other tanks", `Sample#`, fixed = TRUE)) %>%
        filter(!grepl("Sample#", `Sample#`, fixed = TRUE)) %>%
        remove_empty_rows %>%
        rename(sample = `Sample#`,
               day = `Day (In fermentation)`,
               series = `Sample Series` ,
               sample_point = `Sample point`,
               depth = `depth [m] (from liquid level)`
               ) %>%
        filter(!grepl("blank", Time, ignore.case = TRUE)) %>% # remove blanks
        filter(!grepl("blank", Tank, ignore.case = TRUE)) %>%

        mutate_at(vars(Date), funs(as.numeric)) %>%
        mutate_at(vars(Date), funs(as.Date(.,origin="1899-12-30"))) %>%
        mutate_at(vars(Time), funs(as.numeric)) %>%
        mutate_at(vars(Time), funs(as.POSIXct(.*24*60*60, origin=Date, tz = "Europe/Paris")-hours(2))) %>% # we need to remove 2 hours to fix time zones
        mutate(C = ifelse(is.na(C), 0,C)) %>% # fix a missing value
        mutate_at(vars(A, B, C), funs(as.numeric)) %>%
        mutate_at(vars(A, B, C), funs(as.logical)) %>%
        rowwise %>% # make factor saying for what was sampled
        mutate(sampled = paste(c("[D/R]NA","Headspace","Chemical")[c(A,B,C)], collapse=" / ")) %>%
        ungroup %>%
        mutate_at(vars(sampled), funs(as.factor)) %>%
        select(-A, -B, -C) %>%
        mutate(sample_point = factor(sample_point, levels = c("1", "1.5", "2", "3", "3.5", "4", "4.5", "bottom valve", "Bottom higher valve", "bottom", "mid/bottom", "mid", "top/mid", "top"))) %>% # reorder factor
        mutate(series_l = map(series, ~strsplit(..1,"&|\\+") %>% unlist %>% trimws)) %>%
        mutate(Time_norm = period(day=days(day), hour = hour(Time), minute = minute(Time)))


